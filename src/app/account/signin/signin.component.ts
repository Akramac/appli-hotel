import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'kt-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  isSignedIn=false;
  errorMessage="";
  successMessage="";
  public form: FormGroup;
    constructor(private fb: FormBuilder, private router: Router,public firebaseService:FirebaseService,
    private toastrService: ToastrService) {}

    ngOnInit() {
      this.form = this.fb.group({
        uname: [null, Validators.required],
        password: [null, Validators.required]
      });

      if(localStorage.getItem('user') !=null){
        this.isSignedIn=  true;

      }else{
        this.isSignedIn=false
      }
    }


    async onSignIn(email:string,password:string){
      await this.firebaseService.signIn(email,password).then(res => {
        console.log(res);
        this.errorMessage = "";
        this.successMessage = "Login avec succés";
        if(this.firebaseService.isLoggedIn){
          this.isSignedIn=true;
          this.router.navigate(['dash-board']);
        }
      }, err => {
        console.log(err);
        this.errorMessage = err.message;
        this.successMessage = "";
      });


    }

}
