import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { FirebaseService } from '../../services/firebase.service';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'kt-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  isSignedIn=false;
  public form: FormGroup;
  errorMessage="";
  successMessage="";
    constructor(private fb: FormBuilder, private router: Router,public firebaseService:FirebaseService) {}

    ngOnInit() {
      this.form = this.fb.group({
        uname: [null, Validators.required],
        password: password,
        confirmPassword: confirmPassword
      });
    }

    async onSignUp(email:string,password:string,repeatPassword:string){
      if (password!=repeatPassword){
        this.errorMessage = "Mot de passe non identique";
        this.successMessage = "";
      }else{
      await this.firebaseService.signup(email,password);
;
      if(this.firebaseService.isLoggedIn){
        this.isSignedIn=true;
        this.router.navigate(['dashboard']);

      }
    }
  }
}
