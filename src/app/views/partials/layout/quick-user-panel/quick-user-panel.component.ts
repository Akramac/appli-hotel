// Angular
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
// Layout
import { OffcanvasOptions } from '../../../../core/_base/layout';
import { AppState } from '../../../../core/reducers';
import { currentUser, Logout, User } from '../../../../core/auth';

import { FirebaseService } from '../../../../services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-quick-user-panel',
  templateUrl: './quick-user-panel.component.html',
  styleUrls: ['./quick-user-panel.component.scss']
})
export class QuickUserPanelComponent implements OnInit {
  user$: Observable<User>;
  // Public properties
  offcanvasOptions: OffcanvasOptions = {
    overlay: true,
    baseClass: 'offcanvas',
    placement: 'right',
    closeBy: 'kt_quick_user_close',
    toggleBy: 'kt_quick_user_toggle'
  };
  isSignedIn=false;
  userMail:any;
  constructor(private store: Store<AppState>, private router: Router,public firebaseService:FirebaseService) {

  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.user$ = this.store.pipe(select(currentUser));
    if(localStorage.getItem('user') !=null){
      this.userMail=(JSON.parse(localStorage.getItem('user')))['email'];
      this.isSignedIn=  true;

    }else{
      this.isSignedIn=false
    }
  }

  /**
   * Log out
   */
  logout() {

    this.firebaseService.logout();
    this.router.navigate(['signin']);
  }
}
